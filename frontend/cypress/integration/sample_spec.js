Cypress.Commands.add('randomUser', () => {
  const user = `user${Math.floor(Math.random() * 1000000)}`
  cy.get('input[name="username"]').type(user, { force: true })
})

Cypress.Commands.add('inputValueToField', (_inputName, value) => {
  cy.get(`input[name=${_inputName}]`).type(value, { force: true })
})

const inputName = [
  "username",
  "email",
  "password",
  "password1",
  "phone_number",
  "country",
  "city",
  "street_address"
]

const inputDict = {
  "email": ["test@test.com", "test"],
  "password": ["sdf898sf98f9sffsh9s_$$$$", "123456"],
  "password1": ["sdf898sf98f9sffsh9s_$$$$", "123456"],
  "phone_number": ["123456", "abcdefghijklmnop"],
  "country": ["norway", "123456"],
  "city": ["trondheim", "123456"],
  "street_address": ["hoyskoleringen", "123456"]
}

Cypress.Commands.add('runTest', (a, b, c, d, e, f, g, h, status) => {
  cy.visit('/register.html')
  if (!a) {
    cy.randomUser()
  } else {
    cy.inputValueToField(inputName[0], " ")
  }
  cy.inputValueToField(inputName[1], inputDict[inputName[1]][b])
  cy.inputValueToField(inputName[2], inputDict[inputName[2]][c])
  cy.inputValueToField(inputName[3], inputDict[inputName[3]][d])
  cy.inputValueToField(inputName[4], inputDict[inputName[4]][e])
  cy.inputValueToField(inputName[5], inputDict[inputName[5]][f])
  cy.inputValueToField(inputName[6], inputDict[inputName[6]][g])
  cy.inputValueToField(inputName[7], inputDict[inputName[7]][h])
  cy.intercept({ method: 'POST', url: '/api/users/' }).as('registerRequest')
  cy.get('#btn-create-account').click()
  cy.wait('@registerRequest').its('response.statusCode').should('eq', status)
  cy.visit('/logout.html')
})

describe('Two way domain tests', () => {
  before(() => {
    cy.visit('/index.html')
  })
  it('all valid variables', () => {
    cy.runTest(0, 0, 0, 0, 0, 0, 0, 0, 201)
  })
  it('valid username, invalid peer variable', () => {
    cy.runTest(0, 1, 0, 0, 0, 0, 0, 0, 400)
    cy.runTest(0, 0, 1, 0, 0, 0, 0, 0, 400)
    cy.runTest(0, 0, 0, 1, 0, 0, 0, 0, 400)
    cy.runTest(0, 0, 0, 0, 1, 0, 0, 0, 201)
    cy.runTest(0, 0, 0, 0, 0, 1, 0, 0, 201)
    cy.runTest(0, 0, 0, 0, 0, 0, 1, 0, 201)
    cy.runTest(0, 0, 0, 0, 0, 0, 0, 1, 201)
  })
  it('invalid username, invalid peer variable', () => {
    cy.runTest(1, 1, 0, 0, 0, 0, 0, 0, 400)
    cy.runTest(1, 0, 1, 0, 0, 0, 0, 0, 400)
    cy.runTest(1, 0, 0, 1, 0, 0, 0, 0, 400)
    cy.runTest(1, 0, 0, 0, 1, 0, 0, 0, 400)
    cy.runTest(1, 0, 0, 0, 0, 1, 0, 0, 400)
    cy.runTest(1, 0, 0, 0, 0, 0, 1, 0, 400)
    cy.runTest(1, 0, 0, 0, 0, 0, 0, 1, 400)
  })
  it('valid email, invalid peer variable', () => {
    cy.runTest(1, 0, 0, 0, 0, 0, 0, 0, 400)
    // cy.runTest(0,0,1,0,0,0,0,0,400) done allready
    // cy.runTest(0,0,0,1,0,0,0,0,400) done allready
    // cy.runTest(0,0,0,0,1,0,0,0,201) done allready
    // cy.runTest(0,0,0,0,0,1,0,0,201) done allready
    // cy.runTest(0,0,0,0,0,0,1,0,201) done allready
    // cy.runTest(0,0,0,0,0,0,0,1,201) done allready
  })
  it('invalid email, invalid peer variable', () => {
    //cy.runTest(1,1,0,0,0,0,0,0,400) done allready
    cy.runTest(0, 1, 1, 0, 0, 0, 0, 0, 400)
    cy.runTest(0, 1, 0, 1, 0, 0, 0, 0, 400)
    cy.runTest(0, 1, 0, 0, 1, 0, 0, 0, 400)
    cy.runTest(0, 1, 0, 0, 0, 1, 0, 0, 400)
    cy.runTest(0, 1, 0, 0, 0, 0, 1, 0, 400)
    cy.runTest(0, 1, 0, 0, 0, 0, 0, 1, 400)
  })
  it('valid password, invalid peer variable', () => {
    // cy.runTest(1,0,0,0,0,0,0,0,400) done allready
    // cy.runTest(0,1,0,0,0,0,0,0,400) done allready
    // cy.runTest(0,0,0,1,0,0,0,0,400) done allready
    // cy.runTest(0,0,0,0,1,0,0,0,400) done allready
    // cy.runTest(0,0,0,0,0,1,0,0,400) done allready
    // cy.runTest(0,0,0,0,0,0,1,0,400) done allready
    // cy.runTest(0,0,0,0,0,0,0,1,400) done allready
  })
  it('invalid password, invalid peer variable', () => {
    // cy.runTest(1,0,1,0,0,0,0,0,400) done allready
    // cy.runTest(0,1,1,0,0,0,0,0,400) done allready
    cy.runTest(0, 0, 1, 1, 0, 0, 0, 0, 400)
    cy.runTest(0, 0, 1, 0, 1, 0, 0, 0, 400)
    cy.runTest(0, 0, 1, 0, 0, 1, 0, 0, 400)
    cy.runTest(0, 0, 1, 0, 0, 0, 1, 0, 400)
    cy.runTest(0, 0, 1, 0, 0, 0, 0, 1, 400)
  })
  it('valid password1, invalid peer variable', () => {
    // cy.runTest(1,0,0,0,0,0,0,0,400) done allready
    // cy.runTest(0,1,0,0,0,0,0,0,400) done allready
    // cy.runTest(0,0,1,0,0,0,0,0,400) done allready
    // cy.runTest(0,0,0,0,1,0,0,0,400) done allready
    // cy.runTest(0,0,0,0,0,1,0,0,400) done allready
    // cy.runTest(0,0,0,0,0,0,1,0,400) done allready
    // cy.runTest(0,0,0,0,0,0,0,1,400) done allready
  })
  it('invalid password1, invalid peer variable', () => {
    // cy.runTest(1,0,0,1,0,0,0,0,400) done allready
    // cy.runTest(0,1,0,1,0,0,0,0,400) done allready
    // cy.runTest(0,0,1,1,0,0,0,0,400) done allready
    cy.runTest(0, 0, 0, 1, 1, 0, 0, 0, 201)
    cy.runTest(0, 0, 0, 1, 0, 1, 0, 0, 201)
    cy.runTest(0, 0, 0, 1, 0, 0, 1, 0, 201)
    cy.runTest(0, 0, 0, 1, 0, 0, 0, 1, 201)
  })
})

