Cypress.Commands.add('login', (username, password) => {
    cy.intercept({ method: "POST", url: "/api/token/" }).as("login")
    cy.visit("/login.html")
    cy.get('input[name="username"]').type(username)
    cy.get('input[name="password"]').type(password)
    cy.get('#btn-login').click()
    cy.wait("@login")
})

Cypress.Commands.add("logout", () => {
    cy.visit("/logout.html");
})


describe('FR5', () => {
    before(() => {
        cy.visit('/index.html')
    })
    it('', () => {
        cy.login("user1", "NotSecured123")

        cy.visit("/workouts.html");

        cy.intercept({ method: "POST", url: "/api/workouts/" }).as("createWorkout")
        cy.intercept({ method: "GET", url: "/api/workouts/**" }).as("getWorkouts")

        // Create workout 0

        cy.get("#btn-create-workout").click()
        cy.get("#inputName").type("New workout coach")
        cy.get("#inputDateTime").type("2022-05-01T16:00")
        cy.get("#inputNotes").type("here are some notes")
        cy.get("#inputVisibility").select("Coach")
        cy.get('#btn-ok-workout').click()
        cy.wait("@createWorkout")

        cy.url().should("contain", "workouts.html")

        cy.wait("@getWorkouts")

        cy.get("#div-content").children().should("have.length", 0) // Mark: !! This is a special case
        // as the app does not show coach visible workouts to the current user :()

        // Create workout 1

        cy.get("#btn-create-workout").click()
        cy.get("#inputName").type("New workout priv")
        cy.get("#inputDateTime").type("2022-05-01T16:00")
        cy.get("#inputNotes").type("here are some notes")
        cy.get("#inputVisibility").select("Private")
        cy.get('#btn-ok-workout').click()
        cy.wait("@createWorkout")

        cy.url().should("contain", "workouts.html")

        cy.wait("@getWorkouts")

        cy.get("#div-content").children().should("have.length", 0)

        // Create workout 2

        cy.get("#btn-create-workout").click()
        cy.get("#inputName").type("New workout pub")
        cy.get("#inputDateTime").type("2022-05-01T16:00")
        cy.get("#inputNotes").type("here are some notes")
        cy.get("#inputVisibility").select("Public")
        cy.get('#btn-ok-workout').click()
        cy.wait("@createWorkout")

        cy.url().should("contain", "workouts.html")

        cy.wait("@getWorkouts")

        cy.get("#div-content").children().should("have.length", 1)
        cy.get("#div-content").contains("New workout pub")

        cy.intercept({ method: "GET", url: "/api/workouts/2/" }).as("workoutDetail")
        cy.visit("/workout.html?id=2")
        cy.wait("@workoutDetail")

        cy.logout()

    })
    it("Can see correct amount of workouts", () => {

        cy.login("user2", "NotSecured123")
        cy.visit("/workouts.html")
        cy.get("#div-content").contains("New workout pub")
        cy.get("#div-content").contains("New workout coach")
        cy.get("#div-content").children().should("have.length", 2)


        cy.intercept({ method: "GET", url: "/api/workouts/2/" }).as("workoutDetail")
        cy.visit("/workout.html?id=2")
        cy.wait("@workoutDetail").its('response.statusCode').should('eq', 200) // If you are a proper software engineer - please migitage this :))

        cy.logout()
        cy.visit("/index.html")
        cy.login("user3", "NotSecured123")

        cy.intercept({ method: "GET", url: "/api/workouts/2/" }).as("workoutDetail")
        cy.visit("/workout.html?id=2")
        cy.wait("@workoutDetail").its('response.statusCode').should('eq', 403) // YEY!!
    })
})
