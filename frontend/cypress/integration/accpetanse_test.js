Cypress.Commands.add('login', (username, password) => {

    cy.intercept({ method: "POST", url: "/api/token/" }).as("login")
    cy.visit("/login.html")
    cy.get('input[name="username"]').type(username)
    cy.get('input[name="password"]').type(password)
    cy.get('#btn-login').click()
    cy.wait("@login")
})

Cypress.Commands.add("logout", () => {
    cy.visit("/logout.html");
})

describe('acceptance', () => {
    before(() => {
        cy.visit('/index.html')



    })
    it('Can see a list of users', () => {


        cy.intercept({ method: "GET", url: "/api/users/?user=current" }).as("currentUser")

        cy.intercept({ method: "PUT", url: "/api/uservisibility/**" }).as("updateVisibility")

        cy.intercept({ method: "POST", url: "/api/usersearch/" }).as("userSearch")
        cy.get("#user-search").type("user")
        cy.wait("@userSearch")

        cy.get("#search-result-result").children().should("have.length", 0)

        cy.login("user1", "NotSecured123")

        cy.intercept({ method: "GET", url: "/api/users/?user=current" }).as("currentUser")

        cy.visit("/myprofile.html")

        cy.wait("@currentUser")
        cy.wait("@currentUser")

        cy.intercept({ method: "PUT", url: "/api/uservisibility/**" }).as("updateVisibility")
        cy.get("#myprofile-div").children().eq(0).click()
        cy.wait("@updateVisibility")


        cy.visit("/index.html")
        cy.get("#user-search").type("user")
        cy.wait("@userSearch")

        cy.get("#search-result-result").children().should("have.length", 1)


        cy.login("user2", "NotSecured123")

        cy.intercept({ method: "GET", url: "/api/users/?user=current" }).as("currentUser")

        cy.visit("/myprofile.html")

        cy.wait("@currentUser")
        cy.wait("@currentUser")

        cy.get("#myprofile-div").children().eq(0).click()
        cy.wait("@updateVisibility")

        cy.visit("/index.html")
        cy.get("#user-search").type("user")
        cy.wait("@userSearch")

        cy.get("#search-result-result").children().should("have.length", 2)

        // nullstill
        cy.login("user2", "NotSecured123")
        cy.visit("/myprofile.html")

        cy.wait("@currentUser")
        cy.wait("@currentUser")

        cy.get("#myprofile-div").children().eq(0).click()
        cy.wait("@updateVisibility")

        cy.login("user1", "NotSecured123")

        // nullstill
        cy.visit("/myprofile.html")

        cy.wait("@currentUser")
        cy.wait("@currentUser")

        cy.get("#myprofile-div").children().eq(0).click()
        cy.wait("@updateVisibility")


    })
})