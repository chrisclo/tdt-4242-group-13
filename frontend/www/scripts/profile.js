
const getUsername = () => {
    return document.getElementById("username");
}

const error = `KUNNE IKKE HENTE BRUKER`

const fetchProfileByBadSearchForExcactUsername = async (username) => { // I just want to say really sorry to anyone reading this:):) time is limited
    const result = await sendRequest("POST", `${HOST}/api/usersearch/`, { username }).then(r => r.json()) // since the user has landed here - the profile is probably fetchable
    // also the user is not a summary, so why not just use the first one (sorry sorry) :upside_down_emoji: 
    // don't fix it unless its broken? i guess

    if (result.length !== 1) {
        getUsername().text = error;
        return;
    }

    console.log("setting new username");

    getUsername().innerHTML = result[0].username;
}

window.addEventListener("DOMContentLoaded", async () => {
    const currentUrl = new URL(window.location.href);
    const username = currentUrl.searchParams.get("username");

    fetchProfileByBadSearchForExcactUsername(username);
})