

const getSearchResult = () => {
    return document.getElementById("search-result-module");
}

const getSearchResultLoading = () => {
    return document.getElementById("search-result-loader");
}

const getSearchResultDiv = () => {
    return document.getElementById("search-result-result");
}

class SearchBar extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {

        let timeout;

        this.innerHTML = `
        <div>
            <input type="text" id="user-search" placeholder="Search for username or user id" />
            <search-result />
        </div>
        `;

        document.getElementById("user-search").addEventListener("input", (e) => {
            console.log("value updated");
            getSearchResult().style.display = "block";
            getSearchResultLoading().style.display = "block";
            getSearchResultDiv().style.display = "none";
            clearTimeout(timeout);
            timeout = setTimeout(() => {
                searchForUser(e.target.value);
            }, 700);
        })
    }
}


const closeSearchModal = () => {
    getSearchResult().style.display = "none";
}

const searchForUser = async (username) => {

    getSearchResultLoading().style.display = "none";

    console.log("searching for user: ", username);
    const result = await sendRequest("POST", `${HOST}/api/usersearch/`, { username }).then(r => r.json());

    const resultDiv = document.createElement("div");
    resultDiv.classList.add("search-result-wrapper");

    if (result.length === 0) {
        console.log("reuslt is empty");
        resultDiv.appendChild(document.createTextNode("Could not find any users matching the given search"));
    }

    result.forEach((user) => {

        const child = document.createElement("a");
        child.href = "/profile.html?username=" + user.username
        child.classList.add("search-result-child");
        child.text = user.username
        resultDiv.appendChild(child);
    });

    getSearchResultDiv().innerHTML = resultDiv.innerHTML;
    getSearchResultDiv().style.display = "block";

}

customElements.define('search-bar', SearchBar);