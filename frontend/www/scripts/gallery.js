let goBackButton;
import { retrieveWorkoutImages } from './gallery/retriveWorkoutImage'

function handleGoBackToWorkoutClick() {
    const urlParams = new URLSearchParams(window.location.search);
    const id = urlParams.get('id');
    window.location.replace(`workout.html?id=${id}`);
}

window.addEventListener("DOMContentLoaded", async () => {

    goBackButton = document.querySelector("#btn-back-workout");
    goBackButton.addEventListener('click', handleGoBackToWorkoutClick);

    const urlParams = new URLSearchParams(window.location.search);
    const id = urlParams.get('id');
    let workoutData = await retrieveWorkoutImages(id);

});