class SearchResult extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {
        this.innerHTML = `
        <div class="search-result-module" style="display: none;" id="search-result-module">
        <button onClick="closeSearchModal()">Close</button>
        <div id="search-result-loader">Loading...</div>
        <div id="search-result-result" style="display: none;">
        Result
        </div>
        </div>
        `
    }
}

customElements.define("search-result", SearchResult);
