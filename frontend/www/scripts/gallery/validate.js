
async function validateImgFileType(id, host_variable, acceptedFileTypes) {
    let file = await sendRequest("GET", `${host_variable}/api/workout-files/${id}/`);
    let fileData = await file.json();
    let fileType = fileData.file.split("/")[fileData.file.split("/").length - 1].split(".")[1];

    return acceptedFileTypes.includes(fileType);
}

module.exports = { validateImgFileType }