
const error = `KUNNE IKKE HENTE BRUKER`

const getMyProfileDiv = () => document.getElementById("myprofile-div");
const getMyUsername = () => document.getElementById("mp-username");

const fetchProfileProperties = async () => {
    getMyProfileDiv().innerHTML = ``;
    const currentUser = await getCurrentUser()
    getMyUsername().innerHTML = `Hey, ${currentUser.username}`;
    let visibility;
    let text;
    if (currentUser.profile_visible) {
        visibility = false;
        text = "Hide profile from other users"
    } else {
        visibility = true;
        text = "Show profile to other users"
    }
    const button = document.createElement("button");
    button.appendChild(document.createTextNode(text));
    button.onclick = (e) => {
        getMyProfileDiv().innerHTML = "Loading..."
        sendRequest("PUT", `${HOST}/api/uservisibility/${currentUser.id}/`, { profile_visible: visibility }).then(r => {
            window.location.href = "/myprofile.html";
        })
    }
    getMyProfileDiv().appendChild(button);
}


window.addEventListener("DOMContentLoaded", async () => {
    fetchProfileProperties();
})