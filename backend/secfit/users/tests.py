
# This holds transactions
from unicodedata import name
import pytest
import json
from django_mock_queries.query import MockSet
from mock import patch, MagicMock
from rest_framework.test import APIRequestFactory, force_authenticate
from rest_framework.request import Request
from rest_framework import serializers
from unittest.mock import DEFAULT
from django.test import TestCase

# unittest will remove the overhead used for transactions and flushing
# databases
from unittest import TestCase as UnitTestCase

from users.permissions import IsAthlete, IsCoach, IsCurrentUser
from users.serializers.UserSerializer import UserSerializer
from users.models import User, athlete_directory_path
from users.views.user import UserSearch


VALIDATED_PASSWORD = "lasldkfj23113!adsfvsødkfjsadfVASDFAødklsajdfa8313"

DEFAULT_VALIDATED_DATA = {
    'username': 'user',
    'country': 'NORWAY',
    'city': 'Trondheim',
    'street_address': 'Høgskoleringen 5',
    "email": "some@email.com",
    'phone_number': '12341234',
    'password': VALIDATED_PASSWORD,
    'password1': VALIDATED_PASSWORD,
    "athlete_files": [],
    "coach_files": [],
    "workouts": [],
    "athletes": []}


factory = APIRequestFactory()
request = factory.get('/')


serializer_context = {
    'request': Request(request),
}


def fieldShouldBeRequired(user, field):

    class TestException(BaseException):
        def __init__(self, msg):
            self.msg = msg

    try:
        return user.errors[field][0].code == "required"
    except BaseException:
        raise TestException("Field not required")


def shouldThrow(method, arguments):
    try:
        method(arguments)
        return False
    except BaseException:
        return True


class UserSerializationUnitTest(UnitTestCase):
    """
        Unit tests
    """
    @pytest.mark.django_db
    def test_email_must_be_formatted_as_email(self):

        data = DEFAULT_VALIDATED_DATA.copy()
        data['email'] = 'some_email_thats_not_an_email'

        user = UserSerializer(data=data)
        self.assertFalse(user.is_valid())

    def test_required_data_must_be_provided(self):
        user = UserSerializer(data={})
        self.assertFalse(user.is_valid())

        # NEW NON REQUIRED FIELDS MUST BE PROVIDED HERE
        # These fields are actually requrired, but due to django magic they are not
        # flagged as required :poop:
        nonRequiredFields = [
            "url",
            "id",
            "email",
            "phone_number",
            "country",
            "city",
            "street_address",
            "coach"]

        for field in UserSerializer.Meta.fields:
            if (field not in nonRequiredFields):
                self.assertTrue(fieldShouldBeRequired(user, field))

        for field in nonRequiredFields:
            self.assertTrue(shouldThrow(fieldShouldBeRequired, (user, field)))

    @pytest.mark.django_db
    def test_serializer_with_funcitonal_data(self):
        user1 = UserSerializer(
            data=DEFAULT_VALIDATED_DATA.copy(),
            context=serializer_context
        )

        self.assertTrue(user1.is_valid())


class UserSerializationTest(TestCase):
    """
    Integrations tests
    """

    def test_username_must_be_unique(self):
        user1 = UserSerializer(
            data=DEFAULT_VALIDATED_DATA,
            context=serializer_context
        )

        self.assertTrue(user1.is_valid())
        createdUser = user1.create(user1.validated_data)

        self.assertTrue(createdUser.username ==
                        user1.validated_data['username'])

        self.assertTrue(User.objects.get(username="user"))

        user2 = UserSerializer(data=DEFAULT_VALIDATED_DATA.copy())
        self.assertFalse(user2.is_valid())
        self.assertEqual(user2.errors['username'][0].title(
        ).lower(), "A user with that username already exists.".lower())

    def test_valid_password_validation(self):
        data = DEFAULT_VALIDATED_DATA.copy()
        data['password1'] = "soemthing else"

        user = UserSerializer(data=data)
        # This is a result of very bad implementation
        self.assertTrue(user.is_valid())
        self.assertFalse(user.validate_password(False))

    def test_invalid_password_validation(self):
        data = DEFAULT_VALIDATED_DATA.copy()
        data['password'] = "username"
        user = UserSerializer(data=data)
        self.assertRaises(
            serializers.ValidationError,
            user.validate_password,
            False)


class TestCurrentUserPermissions(TestCase):

    def setUp(self):
        self.permission = IsCurrentUser()

        self.request = MagicMock(user=MagicMock())
        self.view = MagicMock()

    def test_user_must_be_current_user(self):
        user = MagicMock()
        self.request.user = user
        self.assertTrue(self.permission.has_object_permission(
            self.request, self.view, user))

        anotherUser = MagicMock()

        self.assertFalse(self.permission.has_object_permission(
            self.request, self.view, anotherUser
        ))


# Pattern used is inspired by:
# https://stackoverflow.com/questions/37170911/how-to-unit-test-permissions-in-django-rest-framework
class TestIsAthletePermissions(TestCase):
    def setUp(self):
        self.permission = IsAthlete()

        self.request = MagicMock()
        self.view = MagicMock()

    def test_atlhete_permissions(self):
        athlete_id = "athlete_id"
        user = MagicMock()
        user.athlete = athlete_id
        self.request.user = user
        athleteUser = MagicMock()
        athleteUser.athlete = user
        self.assertTrue(self.permission.has_object_permission(
            self.request, self.view, athleteUser)
        )

        anotherUser = MagicMock()

        self.assertFalse(self.permission.has_object_permission(
            self.request, self.view, anotherUser
        ))

        self.assertFalse(self.permission.has_object_permission(
            self.request, self.view, user
        ))

    def test_athlete_list_permissions(self):
        user = MagicMock()
        user.id = "1234"
        request = MagicMock()
        request.data = {"athlete": user.id + "/"}
        request.method = "POST"
        request.user = user

        user2 = MagicMock()
        request2 = MagicMock()
        request2.data = {'athlete': user2.id + "/"}
        request2.method = "POST"
        request2.user = user2

        self.assertTrue(self.permission.has_permission(request, self.view))
        self.assertFalse(self.permission.has_permission(request2, self.view))

    def test_athelte_has_permission_when_not_post(self):

        methods = ["GET", "PUT", "PATCH"]

        for method in methods:
            request = MagicMock()
            request.method = method

            self.assertTrue(
                self.permission.has_permission(request, self.view))


class IsCoachPermissions(TestCase):
    def setUp(self):
        self.permission = IsCoach()
        self.request = MagicMock()
        self.view = MagicMock()

    def test_has_obj_permission(self):
        coach = "coach"
        self.request.user = coach
        athleteUser = MagicMock()
        athleteUser.athete = MagicMock()
        athleteUser.athlete.coach = coach

        self.assertTrue(self.permission.has_object_permission(
            self.request, self.view, athleteUser))
        self.assertFalse(self.permission.has_object_permission(
            self.request, self.view, MagicMock()))

    def test_is_coach_has_permissions(self):

        coachUser = User.objects.create(username="coach")
        athleteUser = User.objects.create(username="athlete")
        athleteUser.coach = coachUser
        athleteUser.save()

        request = MagicMock()
        request.method = "POST"
        request.user = coachUser
        request.data = {"athlete": str(athleteUser.id) + "/"}

        self.assertTrue(self.permission.has_permission(request, self.view))

        maliciousUser = User.objects.create(username="bad_guy")

        request.data = {"athlete": str(maliciousUser.id) + "/"}

        self.assertFalse(self.permission.has_permission(request, self.view))

    def test_has_permission_for_other_methods(self):
        methods = ["GET", "PUT", "PATHC"]

        for method in methods:
            request = MagicMock()
            request.method = method

            self.assertTrue(
                self.permission.has_permission(request, self.view))


class ImplementedFeaturesIntegrationTest(TestCase):
    """
    Integration test
    """

    def setUp(self):
        self.factory = APIRequestFactory()
        self.user = User.objects.create_user(
            username='admin', email='test@example.com', password='very_secret')

    def generateUserSearchRequest(self, username):
        request = self.factory.post('/api/usersearch/',
                                    json.dumps({"username": username}),
                                    content_type='application/json')

        force_authenticate(request, user=self.user)

        return request

    def searchResponse(self, request, *args, **kwargs):
        return UserSearch.as_view()(request, *args, **kwargs)

    def doUserSearch(self, *args):
        return self.searchResponse(self.generateUserSearchRequest(*args))

    def test_user_search(self):
        response = self.doUserSearch("admin")
        response.render()
        print(response.data)
        self.assertEquals(response.status_code, 200)
