from django.urls import path, include
from users.views.user import UserList, UserDetail, UserSearch, UserVisibility
from users.views.athlete import AthleteFileList, AthleteFileDetail
from users.views.offer import OfferList, OfferDetail

users_url_patterns = [
    path("", UserList.as_view(), name="user-list"),
    path(
        "<int:pk>/",
        UserDetail.as_view(),
        name="user-detail"),
    path(
        "<str:username>/",
        UserDetail.as_view(),
        name="user-detail"),
]

offers_url_patterns = [
    path(
        "",
        OfferList.as_view(),
        name="offer-list"),
    path(
        "<int:pk>/",
        OfferDetail.as_view(),
        name="offer-detail"),
]

athlete_files_url_patterns = [
    path(
        "",
        AthleteFileList.as_view(),
        name="athlete-file-list"
    ),
    path(
        "<int:pk>/",
        AthleteFileDetail.as_view(),
        name="athletefile-detail",
    ),
]

urlpatterns = [
    path("users/", include(users_url_patterns)),
    path("offers/", include(offers_url_patterns)),
    path("athelete-files/", include(athlete_files_url_patterns)),
    path("usersearch/", UserSearch.as_view(), name="user-search"),
    path(
        "uservisibility/<int:pk>/",
        UserVisibility.as_view(),
        name="user-visibility"),
]
