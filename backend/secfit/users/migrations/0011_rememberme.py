# Generated by Django 3.1 on 2022-04-02 13:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0010_user_profile_visible'),
    ]

    operations = [
        migrations.CreateModel(
            name='RememberMe',
            fields=[
                ('id',
                 models.AutoField(
                     auto_created=True,
                     primary_key=True,
                     serialize=False,
                     verbose_name='ID')),
                ('remember_me',
                 models.CharField(
                     max_length=500)),
            ],
        ),
    ]
