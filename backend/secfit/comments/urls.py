from calendar import c
from django.urls import path, include
from comments.views.comments import CommentList, CommentDetail
from comments.views.likes import LikeList, LikeDetail

likes_url_patterns = [
    path("", LikeList.as_view(), name="like-list"),
    path(
        "<int:pk>/",
        LikeDetail.as_view(),
        name="like-detail"),
]

comments_url_patterns = [
    path("", CommentList.as_view(), name="comment-list"),
    path(
        "<int:pk>/",
        CommentDetail.as_view(),
        name="comment-detail"),
]

urlpatterns = [
    path("comments/", include(comments_url_patterns)),
    path("likes/", include(likes_url_patterns)),
]
