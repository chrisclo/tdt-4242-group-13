from django.urls import path, include
from workouts import views
from rest_framework.urlpatterns import format_suffix_patterns

workouts_url_patterns = [
    path(
        "",
        views.WorkoutList.as_view(),
        name="workout-list"),
    path(
        "<int:pk>/",
        views.WorkoutDetail.as_view(),
        name="workout-detail",
    ),
]

exercises_url_patterns = [
    path(
        "",
        views.ExerciseList.as_view(),
        name="exercise-list"),
    path(
        "<int:pk>/",
        views.ExerciseDetail.as_view(),
        name="exercise-detail",
    ),
]

exercise_instances_url_patterns = [
    path(
        "",
        views.ExerciseInstanceList.as_view(),
        name="exercise-instance-list",
    ),
    path(
        "int:pk>/",
        views.ExerciseInstanceDetail.as_view(),
        name="exerciseinstance-detail",
    ),
]

workout_files_url_patterns = [
    path(
        "",
        views.WorkoutFileList.as_view(),
        name="workout-file-list",
    ),
    path(
        "<int:pk>/",
        views.WorkoutFileDetail.as_view(),
        name="workoutfile-detail",
    ),

]

urlpatterns = format_suffix_patterns(
    [
        path("workouts/", include(workouts_url_patterns)),
        path("exercises/", include(exercises_url_patterns)),
        path("exercise-instances/", include(exercise_instances_url_patterns)),
        path("workout-files/", include(workout_files_url_patterns)),
    ]
)
