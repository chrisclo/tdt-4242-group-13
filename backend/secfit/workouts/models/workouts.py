
"""Contains the models for the workouts Django application. Users
log workouts (Workout), which contain instances (ExerciseInstance) of various
type of exercises (Exercise). The user can also upload files (WorkoutFile) .
"""
from django.db import models
from django.contrib.auth import get_user_model


class Workout(models.Model):
    """Django model for a workout that users can log.

    A workout has several attributes, and is associated with one or more exercises
    (instances) and, optionally, files uploaded by the user.

    Attributes:
        name:        Name of the workout
        date:        Date the workout was performed or is planned
        notes:       Notes about the workout
        owner:       User that logged the workout
        visibility:  The visibility level of the workout: Public, Coach, or Private
    """

    name = models.CharField(max_length=100)
    date = models.DateTimeField()
    notes = models.TextField()
    owner = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name="workouts"
    )

    # Visibility levels
    PUBLIC = "PU"  # Visible to all authenticated users
    COACH = "CO"  # Visible only to owner and their coach
    PRIVATE = "PR"  # Visible only to owner
    VISIBILITY_CHOICES = [
        (PUBLIC, "Public"),
        (COACH, "Coach"),
        (PRIVATE, "Private"),
    ]  # Choices for visibility level

    visibility = models.CharField(
        max_length=2, choices=VISIBILITY_CHOICES, default=COACH
    )

    class Meta:
        ordering = ["-date"]

    def __str__(self):
        return self.name
