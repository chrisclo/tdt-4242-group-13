

"""Contains the models for the workouts Django application. Users
log workouts (Workout), which contain instances (ExerciseInstance) of various
type of exercises (Exercise). The user can also upload files (WorkoutFile) .
"""
import os
from django.db import models
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from django.contrib.auth import get_user_model
from .workouts import Workout


def workout_directory_path(instance, filename):
    """Return path for which workout files should be uploaded on the web server

    Args:
        instance (WorkoutFile): WorkoutFile instance
        filename (str): Name of the file

    Returns:
        str: Path where workout file is stored
    """
    return f"workouts/{instance.workout.id}/{filename}"


class OverwriteStorage(FileSystemStorage):

    """Filesystem storage for overwriting files. Currently unused."""

    def get_available_name(self, name, max_length=None):
        """https://djangosnippets.org/snippets/976/
        Returns a filename that's free on the target storage system, and
        available for new content to be written to.

        Args:
            name (str): Name of the file
            max_length (int, optional): Maximum length of a file name. Defaults to None.
        """
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))


class WorkoutFile(models.Model):
    """Django model for file associated with a workout. Basically a wrapper.

    Attributes:
        workout: The workout for which this file has been uploaded
        owner:   The user who uploaded the file
        file:    The actual file that's being uploaded
    """

    workout = models.ForeignKey(
        Workout,
        on_delete=models.CASCADE,
        related_name="files")
    owner = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        related_name="workout_files")
    file = models.FileField(upload_to=workout_directory_path)
