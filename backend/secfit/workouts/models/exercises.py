

"""Contains the models for the workouts Django application. Users
log workouts (Workout), which contain instances (ExerciseInstance) of various
type of exercises (Exercise). The user can also upload files (WorkoutFile) .
"""
from django.db import models
from .workouts import Workout


class Exercise(models.Model):

    """Django model for an exercise type that users can create.

    Each exercise instance must have an exercise type, e.g., Pushups, Crunches, or Lunges.

    Attributes:
        name:        Name of the exercise type
        description: Description of the exercise type
        duration:    Duration of one unit of the exercise
        calories:    Calories spent per minute
        muscleGroup: What major muscle group is used in the exercise
        unit:        Name of the unit for the exercise type (e.g., reps, seconds)
    """

    name = models.CharField(max_length=100)
    description = models.TextField()
    duration = models.IntegerField(default=0)
    calories = models.IntegerField(default=0)
    muscleGroup = models.TextField(default="Legs")
    unit = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class ExerciseInstance(models.Model):
    """Django model for an instance of an exercise.

    Each workout has one or more exercise instances, each of a given type. For example,
    Kyle's workout on 15.06.2029 had one exercise instance: 3 (sets) reps (unit) of
    10 (number) pushups (exercise type)

    Attributes:
        workout:    The workout associated with this exercise instance
        exercise:   The exercise type of this instance
        sets:       The number of sets the owner will perform/performed
        number:     The number of repetitions in each set the owner will perform/performed
    """

    workout = models.ForeignKey(
        Workout, on_delete=models.CASCADE, related_name="exercise_instances"
    )
    exercise = models.ForeignKey(
        Exercise, on_delete=models.CASCADE, related_name="instances"
    )
    sets = models.IntegerField()
    number = models.IntegerField()
