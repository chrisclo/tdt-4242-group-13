from django.urls import path, include
from meals import views
from rest_framework.urlpatterns import format_suffix_patterns

meal_url_patterns = [
    path(
        "",
        views.MealList.as_view(),
        name="meal-list"),
    path(
        "<int:pk>/",
        views.MealDetail.as_view(),
        name="meal-detail",
    ),
]

meal_files_url_patterns = [
    path(
        "",
        views.MealFileList.as_view(),
        name="meal-file-list",
    ),
    path(
        "<int:pk>/",
        views.MealFileDetail.as_view(),
        name="mealfile-detail",
    ),
]

urlpatterns = format_suffix_patterns(
    [
        path("meals/", include(meal_url_patterns)),
        path("meal-files/", include(meal_files_url_patterns)),
    ]
)
