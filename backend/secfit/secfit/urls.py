"""secfit URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from users.views import user
from secfit import views

token_url_patterns = [
    path(
        "",
        TokenObtainPairView.as_view(),
        name="token_obtain_pair"),
    path(
        "refresh/",
        TokenRefreshView.as_view(),
        name="token_refresh"),
]

api_url_patterns = [
    path("", include("workouts.urls")),
    path("", include("users.urls")),
    path("", include("comments.urls")),
    path("", include("meals.urls")),
    path("auth/", include("rest_framework.urls")),
    path("token/", include(token_url_patterns)),
    path(
        "remember_me/",
        user.RememberMe.as_view(),
        name="remember_me"),
]


urlpatterns = [
    path("", views.api_root),
    path("admin/", admin.site.urls),
    path("api/", include(api_url_patterns)),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
