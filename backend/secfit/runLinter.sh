#! /bin/bash

DIFF=$(python3 -m autopep8 --diff --recursive --aggressive .)

if [ "$DIFF" = "" ];
then
    exit 0
else
    echo "Linter failed. See diff:"
    echo $DIFF
    exit 1
fi
