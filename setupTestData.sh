#! /bin/bash

# create user 1
curl -X POST -H 'Content-Type: application/json' -d '{"username":"user1","password":"NotSecured123", "email": "user1@example.com", "password1":"NotSecured123","athlete_files": [], "coach_files": [], "workouts": [], "athletes": [],"phone_number":"12345678","country":"Norway","city":"Trondheim","street_address":"Høgskoleringen 5"}' http://localhost:9090/api/users/

echo ""
# create user 2

curl -X POST -H 'Content-Type: application/json' -d '{"username":"user2","password":"NotSecured123", "email": "user2@example.com", "password1":"NotSecured123","athlete_files": [], "coach_files": [], "workouts": [], "athletes": ["/api/users/1/"],"phone_number":"12345678","country":"Norway","city":"Trondheim","street_address":"Høgskoleringen 5"}' http://localhost:9090/api/users/
echo ""

# get auth creds for user 1
TOKEN=$(curl -X POST -H 'Content-Type: application/json' -d '{"username": "user1", "password": "NotSecured123"}' http://localhost:9090/api/token/ | jq -r ".access")

echo ""

# make user 2 coach of user 1
curl -X PATCH -H 'Content-Type: application/json' -H "authorization: Bearer ${TOKEN}" -d '{"coach":"http://localhost:9090/api/users/3/"}' http://localhost:9090/api/users/2/

echo ""

# create user 3
curl -X POST -H 'Content-Type: application/json' -d '{"username":"user3","password":"NotSecured123", "email": "user3@example.com", "password1":"NotSecured123","athlete_files": [], "coach_files": [], "workouts": [], "athletes": [],"phone_number":"12345678","country":"Norway","city":"Trondheim","street_address":"Høgskoleringen 5"}' http://localhost:9090/api/users/
